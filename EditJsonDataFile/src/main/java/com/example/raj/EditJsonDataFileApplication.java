package com.example.raj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EditJsonDataFileApplication {

	public static void main(String[] args) {
		SpringApplication.run(EditJsonDataFileApplication.class, args);
	}

}
